<?php

use App\Http\Controllers\DisasterController;
use App\Http\Controllers\DistrictsController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('users/store', [UserController::class, 'register']);
Route::post('users/auth', [UserController::class, 'auth']);
Route::post('users/auth/recover/verify', [UserController::class, 'verification']);
Route::post('users/auth/recover', [UserController::class, 'recover']);
Route::post('users/auth/check', [UserController::class, 'authCheck']);
Route::post('districts/get', [DistrictsController::class, 'get']);
Route::post('disasters/store', [DisasterController::class, 'store']);
Route::post('disasters/update/{id}/{status}', [DisasterController::class, 'update']);
Route::post('disasters/get/all', [DisasterController::class, 'getAll']);
Route::post('disasters/get/all/filter/destrict/{id}', [DisasterController::class, 'getDesctrictFilterAll']);
Route::post('disasters/get/{userid}', [DisasterController::class, 'get']);
Route::post('disasters/types/get', [DisasterController::class, 'getDisasterTypes']);
