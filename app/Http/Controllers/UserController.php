<?php

namespace App\Http\Controllers;

use App\Models\JsonResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:2|max:50',
            'nic' => 'required|string|min:2|max:12|unique:users',
            'password' => 'required|string|min:5|max:12',
            'rpassword' => 'required|string|min:5|max:12:same:password',
            'contactno' => 'required|string|min:2|max:10',
            'district' => 'required|integer',
            'address' => 'string|min:2|max:100',
        ]);

        if ($validator->fails()) {
            return (new JsonResponse)->send("Registration Terminated", [], 403);
        }

        $userData = [
            'name' => $request->name,
            'nic' => $request->nic,
            'password' => Hash::make($request->password),
            'contactno' => $request->contactno,
            'districtid' => $request->district,
        ];

        if ($request->has('address')) $userData['address'] = $request->address;

        User::create($userData);

        return (new JsonResponse)->send("Registration Success", [], 200);
    }

    public function auth(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nic' => 'required|string|exists:users',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return (new JsonResponse)->send("Unauthorized Credentials", [], 401);
        } else {
            $userData = User::where('nic', $request->nic)->with('district')->first();
            if ($userData != null) {
                if (Hash::check($request->password, $userData->password)) {
                    return (new JsonResponse)->send("Login Succeed", $userData, 200);
                } else {
                    return (new JsonResponse)->send("Unauthorized Credentials", [], 401);
                }
            }
        }
    }
    
    public function authCheck(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|exists:users'
        ]);

        if ($validator->fails()) {
            return (new JsonResponse)->send("Unauthorized", [], 401);
        } else {
            $userData = User::where('id', $request->id)->with('district')->first();
            if ($userData!=null) {
                return (new JsonResponse)->send("Login Succeed", $userData, 200);
            } else {
                return (new JsonResponse)->send("Unauthorized Credentials", [], 401);
            }
        }
    }
    
        public function verification(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nic' => 'required|string|min:2|max:12|exists:users',
        ]);

        if ($validator->fails()) {
            return (new JsonResponse)->send("Invalid NIC", [], 403);
        }

        $randCode=rand(1000,9000);

        $userData=User::where('nic',$request->nic)->first();

        if($userData!=null){

            $user = "94779778269";
            $password = "4024";
            $text = urlencode("Safe account recover pin : ".$randCode);
            $to = $userData->contactno;


            $baseurl ="http://www.textit.biz/sendmsg";
            $url = "$baseurl/?id=$user&pw=$password&to=$to&text=$text";

            Http::get($url);
        }

        $ret=[
            'pincode'=>$randCode
        ];

        return (new JsonResponse)->send("Verification code sent.", $ret, 200);
    }

    public function recover(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nic' => 'required|string|min:2|max:12|exists:users',
            'password' => 'required|string|min:5|max:12',
            'rpassword' => 'required|string|min:5|max:12:same:password',
        ]);

        if ($validator->fails()) {
            return (new JsonResponse)->send("Recover Terminated", [], 403);
        }

        $userData = [
            'password' => Hash::make($request->password),
        ];

        User::where('nic',$request->nic)->update($userData);

        return (new JsonResponse)->send("Password Recovered", [], 200);
    }
}
