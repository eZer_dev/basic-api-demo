<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\JsonResponse;
use Illuminate\Http\Request;

class DistrictsController extends Controller
{
    public function get()
    {
        return (new JsonResponse)->send("Loaded Successfully", District::get(), 200);
    }
}
