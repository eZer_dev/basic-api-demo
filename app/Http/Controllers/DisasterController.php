<?php

namespace App\Http\Controllers;

use App\Models\Disaster;
use App\Models\DisasterHasImages;
use App\Models\DisasterType;
use App\Models\JsonResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

class DisasterController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'disastertypeid' => 'required|integer|exists:disaster_types,id',
            'details' => 'string',
            'lng' => 'required|string',
            'ltd' => 'required|string',
            'userid' => 'required|integer|exists:users,id',
        ]);

        if ($validator->fails()) {
            return (new JsonResponse)->send("Process Terminated", [], 403);
        } else {

            $disasterData = [
                'disastertypeid' => $request->disastertypeid,
                'lng' => $request->lng,
                'ltd' => $request->ltd,
                'userid' => $request->userid,
            ];

            if ($request->has('details')) $disasterData['details'] = $request->details;

            $disasterRecord = Disaster::create($disasterData);

            if ($request->has('image1')) {
                $image1 = $request->image1;
                $imageName1 = config('app.name') . time() . '1.' . 'jpg';
                $path = public_path('uploads').'/'.$imageName1;
                $image_base64 = base64_decode($image1);
                file_put_contents($path, $image_base64);

                DisasterHasImages::create([
                    'path' =>  $imageName1,
                    'disasterid' => $disasterRecord->id,
                ]);
            }

            if ($request->has('image2')) {
                $image2 = $request->image2;
                $imageName2 = config('app.name') . time() . '2.' . 'jpg';
                $path2 = public_path('uploads').'/'.$imageName2;
                $image_base642 = base64_decode($image2);
                file_put_contents($path2, $image_base642);

                DisasterHasImages::create([
                    'path' =>  $imageName2,
                    'disasterid' => $disasterRecord->id,
                ]);
            }

            if ($request->has('image3')) {
                $image3 = $request->image3;
                $imageName3 = config('app.name') . time() . '3.' . 'jpg';
                $path3 = public_path('uploads').'/'.$imageName3;
                $image_base643 = base64_decode($image3);
                file_put_contents($path3, $image_base643);

                DisasterHasImages::create([
                    'path' =>  $imageName3,
                    'disasterid' => $disasterRecord->id,
                ]);
            }

            return (new JsonResponse)->send("Saved Successfully", Disaster::where('id', $disasterRecord->id)->with('user')->with('images')->with('disasterType')->first(), 200);
        }
    }

    public function update($id, $status, Request $request)
    {
        $updatedRecord = Disaster::where('id', $id)->update([
            'status' => $status
        ]);
        
        $updatedRecord=Disaster::where('id', $id)->with('user')->with('disasterType')->first();
        
        if($status==1){

            $data=User::where('districtid',$updatedRecord->user->district->id)->get();
            

            $user = "94779778269";
            $password = "4024";
            $text = urlencode($updatedRecord->disastertype['type']." "."reported from your district. Please launch application to get more details. - SAFE -");
            $to = "";
            
            $index=0;
            $indexAll=count($data);

            foreach ($data as $value) {
                $to.=$value->contactno;
                $index++;
                if($index<$indexAll){
                    $to.=',';
                }
            }

            $baseurl ="http://www.textit.biz/sendmsg";
            $url = "$baseurl/?id=$user&pw=$password&to=$to&text=$text";
            
            Http::get($url);
        }

        return (new JsonResponse)->send("Updated Successfully", $updatedRecord, 200);
    }

    public function getAll()
    {
        return (new JsonResponse)->send("Loaded Successfully", Disaster::where('status', '!=' , 3)->with('user')->with('images')->with('disasterType')->get(), 200);
    }

    public function get($userid)
    {
        return (new JsonResponse)->send("Loaded Successfully", Disaster::where('status', 1)->where('userid', $userid)->with('user')->with('images')->with('disasterType')->get(), 200);
    }

    public function getDesctrictFilterAll($id)
    {
        return (new JsonResponse)->send("Loaded Successfully", Disaster::where('status', 1)->where('disastertypeid', $id)->with('user')->with('images')->with('disasterType')->get(), 200);
    }

    public function getDisasterTypes()
    {
        return (new JsonResponse)->send("Successfully Loaded", DisasterType::get(), 200);
    }
}
