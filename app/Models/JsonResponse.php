<?php

namespace App\Models;

class JsonResponse
{
    public function send($message,$data,$status)
    {
        $response = [
            "msg" => $message,
            "data" => $data,
            "status" => $status,
        ];

        return response()->json($response, $status);
    }
}
