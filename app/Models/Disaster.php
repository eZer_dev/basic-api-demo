<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disaster extends Model
{
    protected $guarded=[];

    public function user()
    {
        return $this->hasOne('App\Models\User','id','userid')->with('district');
    }

    public function disasterType()
    {
        return $this->hasOne('App\Models\DisasterType','id','disastertypeid');
    }

    public function images()
    {
        return $this->hasMany('App\Models\DisasterHasImages','disasterid','id');
    }
    
    protected $casts = [
        'status' => 'int',
    ];
}
