<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $hidden = [
        'password',
    ];
    
    protected $casts = [
        'districtid' => 'int',
        'isadmin' => 'int',
    ];

    public function district()
    {
        return $this->hasOne('App\Models\District','id','districtid');
    }

}
